\documentclass{article}
\title{MAT157 Notes: Solids and Surfaces of Revolution}
\author{Jad Elkhaleq Ghalayini}
\date{April 2 2018}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage[utf8]{inputenc}
\newtheorem{corollary}{Corollary}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}
\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}

\begin{document}
\maketitle 

\begin{theorem}[Belzano Weierstrass Theorem]
If a sequence is bounded, then it must have a convergent subsequence
\end{theorem}
\begin{proof}
Choose a subsequence that is non-increasing or non-decreasing. It is bounded. So it converges
\end{proof}
\begin{definition}
A sequence is a \underline{Cauchy sequence} iff for any \(\epsilon > 0, \exists N > 0\) such that for any \(m, n > N\), we have \(|a_m - a_n| < \epsilon\)
\end{definition}
\begin{theorem}
A sequence converges iff it is a Cauchy sequence
\end{theorem}
\begin{proof}
\begin{itemize}
    
    \item [\(\implies\):] Suppose \(\{a_n\}\) converges to \(L\). Given \(\epsilon > 0, \exists N\) such that for \(m > N\), \(|a_m - L| < \frac{\epsilon}{2}\). If \(n > N\), then 
    \[|a_n - a_m| \leq |a_n - L| + |L - a_m| < \frac{\epsilon}{2} + \frac{\epsilon}{2} = \epsilon\]
    
    \item [\(\impliedby\):] Suppose \(\{a_n\}\) is Cauchy.
    \begin{claim}
    \(\{a_n\}\) is bounded
    \end{claim}
    \begin{proof}
    Take \(\epsilon = 1\). \(\exists N\) such that for \(m, n > N, |a_m - a_n| < 1\). For any \(m > N\),
    \[|a_m - a_{N + 1}| < 1 \implies |a_m| \leq |a_{N + 1}| + 1\]
    There are only finitely many terms with \(n < N _ 1\), so the sequence is bounded. 
    \end{proof}
    The Bolzano-Weierstrass theorem says there is a convergent subsequence. Suppose
    \[a_{n_1}, a_{n_2}, ..., \to L\]
    Given \(\epsilon > 0, \exists N\) such that \[k > N \implies |a_{n_k} - L| < \frac{\epsilon}{2}\] 
    Also, \(\exists M\) such that
    \[|a_m - a_n| < \frac{\epsilon}{2} \forall m, n > M\]
    If \(R = \max(M, n_{N + 1})\), then for \(m, n > R\), 
    \[|a_m - L| \leq |a_m - a_{n_{R + 1}}| + |a_{n_{R + 1}} - L| < \frac{\epsilon}{2} + \frac{\epsilon}{2} = \epsilon\]
    
\end{itemize}
\end{proof}
Cauchy sequences can be used for an alternative definition of the real numbers, though we won't spend time on this: a Cauchy sequence of rationals can converge to an arbitrary real number.
\section{Series}
Given a sequence \(a_1,a_2,...\) write \(n\)-th partial sum as
\[s_n = \sum_{m = 1}^na_m = s_n\]
(the sum of the first \(n\) terms). If the partial sums \(s_m\) converge to \(S\), we say the series converges and write
\[\sum_{m = 1}^\infty a_n = S\]
i.e.
\[\lim_{n \to \infty}s_n = \lim_{n \to \infty}\sum_{m = 1}^na_m = S\]
We also say that \(\{a_m\}\) are summable. If the partial sums don't converge, we say
\(\sum_{m = 1}^\infty a_n\)
diverges.

If we apply the Cauchy Criterion to the seqeunce of partial sums, we find that \(\sum_{m = 1}^\infty a_m\) converges if and only if
\[\lim_{m, n \to \infty}|s_m - s_n| = 0\]
i.e. 
\[\lim_{m, n \to \infty}|a_{n + 1} + ... + a_{m}| = 0\]
In particular, with \(m = n + 1\), we find that if \(\sum_{n = 1}^\infty a_m\) converges, then
\[\lim_{n \to \infty}|a_n| = 0\]
This is called the ``\(n\)-th term test'' or ``vanishing criterion''
\section{Geometric Series}
Consider the series.
\[1 + r + r^2 + ... + r^n = \frac{1 - r^{n + 1}}{1 - r} = \frac{1}{1 - r} - \frac{r^{n + 1}}{1 - r}\]
What happens as \(r \to \infty\). If \(r = 1\), \(S_n = 1 + 1 + 1 + ... + 1 = n \to \infty\). If \(|r| < 1\),
\[r^{n + 1} \to 0 \implies s_n \to\frac{1}{1 - r}\]
Clearly, if \(|r| > 1\), the series diverges by the \(n\)-th term test. If \(r = -1\), \(\{s_n\} = 1, 0, 1, 0, ...\) diverges.

This is related to Zeno's paradox:
if you want to walk 1 meter, you need to walk 1/2 meters, then 1/4, then 1/8,..., so you never get there. Except with Cauchy sequences, you do. You can use the same idea to show \(0.9999.... = 1\).

\subsection{Harmonic Series}
Consider the harmonic series
\[\sum_{n = 1}^\infty\frac{1}{n} = 1 + \frac{1}{2} + \frac{1}{3} + ...\]
This passes the term test \(\frac{1}{n} \to 0\), but it turns out this sequence does \underline{not} converge. We'll show one reason now, and a neater one later: consider
\[s_m' = \sum_{k = 2^m}^{2^{m + 1}}\frac{1}{k} > \sum_{k = 2^m}^{2^{m + 1}}\frac{1}{2^{m + 1}} = 2^m \cdot \frac{1}{2^{m + 1}} = \frac{1}{2}\]
Hence, since we can write
\[\sum_{n = 1}^\infty\frac{1}{n} = \sum_{n = 1}^\infty s_n'\]
we have that the series diverges.

\end{document}