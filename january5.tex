\documentclass{article}
\title{MAT157 Notes: Integrals}
\author{Jad Elkhaleq Ghalayini}
\date{January 5 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\newtheorem{corollary}{Corollary}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem*{note}{Note}
\begin{document}
\maketitle 

Consider a function \(f(x)\) on \([a, b]\) with \(f(x) \geq 0\). We want to find the area of the region under the curve.

By now, you've probably got the idea that a lot of analysis is approximating things, then taking limits. So that, naturally, is how we're going to approach this problem: approximate it by the sum of the areas of rectangles under the graph. We may all choose different rectangles to do it, but in the end, since we take a limit, we should approach just one value. While we're at it, we could take rectangles above the graph as well, though these give an overestimate instead of an underestimate, so the actual area should be in between these values. Another important issue is where we divide the rectangles. 

\begin{definition}
A \underline{partition} of \([a, b]\) is a finite set \[\mathbb{P} = \{t_0 = a < t_1 < t_2 < ... < t_n = b\}\]
\end{definition}

\underline{Assume} \(f(x)\) is \underline{bounded} on \([a, b]\) and for \(i = 1,...,n\), define 
\[m_i = \inf\{f(x)|t_{i - 1}\leq x \leq t_i\}, M_i = \sup\{f(x)|t_{i - 1} \leq x \leq t_i\}\]
Note we use \(\inf\) and \(\sup\) rather than \(\min\) and \(\max\) as we are \underline{not} assuming the function is continuous.

The rectangle has height \(m_i\) and width \(t_i - t_{i - 1}\), so the area of the rectangle is \(m_i(t_i - t_{i - 1})\).
\begin{definition}
The lower sum \(L(f, \mathbb{P})\) for a partition \(\mathbb{P}\) and function \(f\) is
\[L(f, \mathbb{P}) = \sum_{i = 1}^nm_i(t_i - t_{i - 1})\]
The upper sum \(U(f, \mathbb{P})\) is given by
\[U(f, \mathbb{P}) = \sum_{i = 1}^nM_i(t_i - t_{i - 1})\]
\end{definition}

From the picture, it's obvious that the lower sum is less than the upper sum. From the definition, its still obvious, considering 
\[M_i \geq m_i \implies U(f, \mathbb{P}) \leq L(f, \mathbb{P})\]
Example: \[f(x) = 1 \ \forall \ x \in \mathbb{Q}, f(x) = 0 \ \forall \ x \in \mathbb{R} \setminus \mathbb{Q}\] on \((0, 1)\).
\[\mathbb{P}_n = \left\{0, \frac{1}{n}, \frac{2}{n}, \frac{3}{n}, ..., \frac{n - 1}{n}, 1\right\}\]
We can clearly see, for any interval, \(m_i = 0, M_i = 1\). So
\[L(f, \mathbb{P}_n) = 0, U(f, \mathbb{P}_n) = \sum_{k = 0}^n\frac{1}{n} = 1\]
This is clearly not a good approximation, considering the upper sum is always 1, and the lower sum is always 0.
Consider partition \(Q = \{0, \frac{1}{\sqrt{2}}, 1\}\). We have the same problem!
We'll just have to accept the fact that some functions don't work. We say that a function like this is \textit{not integrable}. What went wrong here is that the lower approximation stayed far away from the upper approximation no matter what partition we used.

In fact, \(L(f, \mathbb{P}) = 0, U(f, \mathbb{P}) = 1\) for \textit{any} partition of \([0, 1]\).

We want to show \(L(f, \mathbb{P}) \leq U(f, Q)\) for \underline{any} partitions \(\mathbb{P}, Q\) of \([a, b]\).

\begin{lemma}
Suppose \(\mathbb{P}' = \mathbb{P} \cup \{u\}, u \neq \mathbb{P}\). Then
\[L(f, \mathbb{P}') \geq L(f, \mathbb{P})\]
\end{lemma}
\begin{proof}
The part of the sum corresponding to \(t_{i - 1} < u < t_{i}\) is
\[\inf_{[t_{i - 1}, t_i]}f\cdot(t_i - t_{i - 1})\]
For \(\mathbb{P}'\), it becomes
\[\inf_{[t_{i - 1}, u]}f\cdot(u - t_{i - 1}) + \inf_{[u, t_i]}f\cdot(t_i - u)\]
We know \[\inf_{[t_{i - 1}, t_i]}f \leq \inf_{[t_{i - 1}, u]}f, \inf_{[u, t_i]}f\] by definition.
So
\[\inf_{[t_{i - 1}, u]}f\cdot(u - t_{i - 1}) + \inf_{[u, t_i]}f\cdot(t_i - u) \leq \inf_{[t_{i - 1}, t_i]}f \cdot (u - t_{i - 1} + t_i - u)\]
\[= \inf_{[t_{i - 1}, t_i]}f \cdot (t_i - t_{i - 1}) \implies L(f, \mathbb{P}) \leq L(f, \mathbb{P}')\]
Since all the other values in the sums \(L(f, \mathbb{P})\) and \(L(f, \mathbb{P}')\) are the same.
\end{proof}
Similarly, \(U(f, \mathbb{P}) \geq U(f, \mathbb{P}')\).
\end{document}