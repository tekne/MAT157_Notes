\documentclass{article}
\title{MAT157 Notes: Techniques of Integration}
\author{Jad Elkhaleq Ghalayini}
\date{February 28 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage[utf8]{inputenc}
\newtheorem{corollary}{Corollary}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem*{note}{Note}
\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}

\begin{document}
\maketitle 

Recall that integration rules yield differentiation rules, and vice versa. We have the product rule
\[(fg)' = f'g + fg' \implies fg = \int(fg)' = \int(f'g) + \int(fg')\]
Rewriting this, we get
\[\int fg' = fg - \int f'g\]
i.e. \underline{integration by parts}.

As an example, take,
\[I = \int xe^xdx\]
If we pick
\[g(x) = \frac{1}{2}x^2, g'(x) = , f(x) = e^x = f'(x)\]
\[I = \frac{1}{2}x^2e^x - \int\frac{1}{2}x^2e^xdx\]
Let's try this the other way around:
\[f(x) = x, f'(x) = 1, g'(x) = g(x) = e^x\]
Hence,
\[I = xe^x - \int e^xdx = xe^x - ex^2 (+ C)\]
Let's generalize this: suppose we have
\[I_n = \int x^ne^{-x}dx\]
We try \(f(x) = x^n, g'(x) = e^{-x}\). In this case,
\[f'(x) = nx^{n - 1}, g'(x) = -e^{-x}\]
So we have that
\[I_n = -x^ne^{-x} + n\int x^{n - 1}e^{-x}dx = -x^ne^{-x} + nI_{n - 1}\]
Since we know \(I_0 = -e^{-x}\), and can hence find any \(I_n\) recursively. Expanding our formula, we quickly obtain
\[I_n = -e^{-x}\sum_{k = 0}^n\frac{n!}{k!}x^k\]
We can prove this by induction
\[I_n = -x^{n}e^{-x} + nI_{n - 1} = -x^{n}e^{-x} -ne^{-x}\sum_{k = 0}^{n - 1}\frac{(n - 1)!}{k!}x^k\]\[ = -x^{n}e^{-x} -e^{-x}\sum_{k = 0}^{n - 1}\frac{n!}{k!}x^k\]
\[= -e^{-x}\sum_{k = 0}^n\frac{n!}{k!}x^k\]
So far, we've used trial and error to pick \(f, g\), but we want to find a more systematic way to do so.
\[\int fg' = fg - \int f'g\]
We want to pick \(f\) so its derivative is simpler than itself, and we want \(g'\) such that it has a nice antiderivative.

Logarithmic functions have nice derivatives, as well as inverse trig functions. On the other hand, exponential functions have especially nice antiderivatives, as do trig functions (sinusoidal). Polynomials (and algebraic functions), would fit in both categories. Hence, we can usually follow the order (inverse trig, logarithms, algebraic, trigonometric, exponential) for choosing \(f\) and (logarithms, inverse trig, polynomials, exponentials and trigonometric) for choosing \(g\).

Let's look at an example where this rule \textit{doesn't} work:
\[\int\frac{x - 1}{x^2}e^xdx\]
Suppose we take the rational function as \(f\), and \(e^x\) as \(g'\). We have
\[f' = \frac{x^2 + 2x(1 - x)}{x^4}\]
which we can simplify, but things overall just get worse. We can try spligging theintegral:
\[\int\frac{x - 1}{x^2}e^xdx = \int\frac{1}{x}e^xdx - \int\frac{1}{x^2}e^xdx\]
The latter component is the exponential integral, which has no closed form. We can still try integration by parts on the first component though, choosing \(g'(x) = 1/x, f(x) = e^x\) and obtaining
\[\int\frac{1}{x}e^xdx = -\frac{1}{x^2}e^x + \int\frac{1}{x^2}e^xdx\]
Substituting this into the above, we have
\[\int\frac{x - 1}{x^2}e^xdx =  -\frac{1}{x^2}e^x + \cancel{\int\frac{1}{x^2}e^xdx - \int\frac{1}{x^2}e^xdx}\]

Let's try
\[\int\ln xdx = \int\log x \cdot 1dx\]
If we choose \(f(x) = \log x \implies f'(x) = \frac{1}{x}, g'(x) = 1 \implies g(x) = 1\), we have
\[\int\log xdx = x\log x - \int 1dx = x\log x - x\]
Remark: in general, for any differentiable \(f\) with a continuous inverse,
\[\int f(x)dx = xf(x) - \int xf'(x)dx = xf(x) - \int f^{-1}(f(x))f'(x)dx\]\[= xf(x) - f^{-1}(f(x))'\]

Another technique is using algebra:
\[I = \int e^x\sin xdx\]
Choosing \(f(x) = e^x, g'(x) = \sin x\) we get
\[I = -e^x\cos x + \int e^x\cos xdx\]
Now choosing \(g'(x) = \cos x\), we get
\[I = -e^x\cos x + e^x\sin x - \int e^x\sin x = -e^x\cos x + e^x\sin x - I\]
\[\implies I = \frac{e^x\sin x - e^x\cos x}{2}\]
\end{document}