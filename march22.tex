\documentclass{article}
\title{MAT157 Notes: Solids and Surfaces of Revolution}
\author{Jad Elkhaleq Ghalayini}
\date{March 22 2018}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage[utf8]{inputenc}
\newtheorem{corollary}{Corollary}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem*{note}{Note}
\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}

\begin{document}
\maketitle 

\begin{lemma}
Suppose \(R\) is \(n + 1\)-times differentiable and
\[R(a) = R'(a)= R''(a) = ... = R^{(n)}(a) = 0\]
Then \(\exists z\) between \(a\) and \(x\) such that
\[\frac{R(x)}{(x - a)^{n + 1}} = \frac{R^{(n + 1)}(z)}{(n + 1)!}\]
\end{lemma}
\begin{proof}
Induction on \(n\):
\begin{itemize}
    \item [Base case:] \(n = 0\) by MVT
    \item [Inductive hypothesis:] Suppose the lemma is true for \(n\)
    \item [Induction step:] The Cauchy Mean Value Theorem tells us that
    \[\frac{R(x)}{(x - a)^{n + 1}} = \frac{R'(t)}{(n + 1)(t - a)^n}\]
    Since \(R'\) is at least \(n\)-times differentiable, we can apply the inductive hypothesis between \(a\) and \(t\) to show that
    \[\exists z \in [a, t], \frac{R'(t)}{(t - a)^n} = \frac{(R')^{(n)}(z)}{n!} = \frac{R^{(n + 1)}(z)}{n!}\]
    So
    \[\frac{R(x)}{(x - a)^{n + 1}} = \frac{R'(t)}{(n + 1)(t - n)^n} = \frac{R^{n + 1}(z)}{(n +1)!}\]
\end{itemize}
\end{proof}
\begin{theorem}[Taylor's Theorem]
Suppose \(f\) is \((n + 1)\)-times differentiable on some open interval containing \(a\). Then for \(x\) in that interval,
\[f(x) = f(a) + f'(a)(x - a) + ... + \frac{f^{(n)}(a)}{n!}(x - a)^n + R_{n,a}(x)\]
and \(\exists z\) between \(x\) and \(a\) such that
\[R_{n,a}(x) = \frac{f^{(n + 1)}(z)}{(n + 1)!}(x - a)^{n + 1}\]
\end{theorem}
This is the Lagrange form of the remainder. It looks like the next term in the Taylor polynomial, \underline{except} that \(f^{(n + 1)}\) is evaluated at \(z\) rather than at \(a\). We now proceed to prove Taylor's theorem:
\begin{proof}
Consider \(f(x) - R_{n, a}(x) = \) Taylor polynomial,
\[R_{n, a}(x) = f(x) - P_{n,a}(x)\]
Hence
\[R_{n,a}(a) = R_{n,a}'(a) = ... = R_{n,a}^{(n)}(a) = 0\]
So by the Lemma, 
\[\exists z \in [x, a], \frac{R_{n, a}(x)}{(x - a)^n} = \frac{R_{(n, a)}^{(n + 1)}(z)}{(n + 1)!}\]
\[\implies R_{n, a}(x) = \frac{R_{n, a}^{(n + 1)}(x)}{(n + 1)!}(x - a)^{n + 1}\]
Note that
\[R_{n, a}(x) - f\]
is a polynomial of degree \(n\), implying that 
\[\frac{d^n}{dx^n}(R_{n, a}(x) - f) = 0 \implies f^{(n + 1)}(x) = R_{n,a}^{(n + 1)}(x)\]
substituting back into the above, we get the desired result
\[\implies R_{n, a}(x) = \frac{f^{(n + 1)}(z)}{(n + 1)!}(x - a)^{n + 1}\]
\end{proof}
We can write out the Taylor expansion about \(x = 0\)
\[e^x = 1 + x + \frac{x^2}{2} + \frac{x^3}{3!} + ... + \frac{x^n}{n!} + R_{n,0}(x)\]
How accurate will \(P_{5, 0}(x)\) be as an estimate for \(e^x\) at \(x = 1\)?
\[R_{5, 0}(1) = \frac{e^z}{6!}\cancel{(1 - 0)^6} = \frac{e^z}{720} < \frac{4}{720} = \frac{1}{180}\]
for some \(z \in (0, 1)\). So we get a pretty good estimate
\[e \approx 1 + 1 + \frac{1}{2} + \frac{1}{6} + \frac{1}{24} + \frac{1}{120} = 2.7167\]
(compared to the value the calculator gives of \(e = 2.71828128\)). Note this shows that \(e < 3\).

Now consider \(\log(x)\). Note that this is undefined at \(x = 0\). Rather than try \(a = 1\), we instead Taylor expand \(f(x) = \frac{1}{1 + x}\) to obtain
\[\frac{1}{1 + x} = 1 - x + x^2 - x^3 + ... + (-1)^nx^n + (-1)^{n + 1}\frac{x^{n + 1}}{1 + x}\]
giving
\[\log(1 + x) = \int_0^x\left(1 - x + x^2 - x^3 + ... + (-1)^nx^n + (-1)^{n + 1}\frac{x^{n + 1}}{1 + x}\right)dx\]
\[= x - \frac{x^2}{2} + \frac{x^3}{3} - \frac{x^4}{4} + ... + (-1)^n\frac{x^{n + 1}}{n + 1} + (-1)^{n + 1}\int_0^x\frac{t^{n + 1}}{1 + t}dt\]
But the remainder doesn't get small as \(n \to \infty\). In this case, it gets massive, except for \(-1 < x < 1\). Even between \(-1\) and \(1\), the remainders get small \underline{very} slowly, implying it takes \underline{many} terms to approximate correctly. Another fun example is
\[g(x) = e^{-1/x^2}\]
You can prove by induction that for all \(n\),
\[g^{(n)}(0) = 0\]
implying every Taylor polynomial is \underline{also} zero, meaning that the remainder is always equal to \(g\). This is a more bizarre example, but both of these show that Taylor polynomials don't always work. To some extent, if a Taylor polynomial does work, we can sort of deduce that the function ``behaves a bit like a polynomial.'' For some things like \(\sin, \cos, \exp\), it works wonderfully. For other things like \(\log\), it sort of works some of the time. For some things, it doesn't work at all.

\end{document}